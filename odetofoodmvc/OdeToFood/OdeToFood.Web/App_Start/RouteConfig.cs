﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using OdeToFood.Data.Services;

namespace OdeToFood.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                // /greetings/
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
        //internal static void RegisterContainer()
        //{
        //    var builder = new ContainerBuilder();
        //    builder.RegisterControllers(typeof(MvcApplication).Assembly); //Custom extension method speifically designed to integrate MvC5 with Autofac. It will scan through project or controller and register those to Autofac.
        //    builder.RegisterType<InMemoryRestaurantData>()
        //        .As<IRestaurantData>()
        //        .SingleInstance();
        //    var container = builder.Build();
        //    DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        //}
    }
}
