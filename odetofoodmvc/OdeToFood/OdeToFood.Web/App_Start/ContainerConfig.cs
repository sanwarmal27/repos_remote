﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using OdeToFood.Data.Services;

namespace OdeToFood.Web
{
    public class ContainerConfig
    {
        internal static void RegisterContainer(HttpConfiguration httpconfiguration)
        {
            var builder = new ContainerBuilder(); //IOC container
            /*Registering different controller  in application                */
            builder.RegisterControllers(typeof(MvcApplication).Assembly); //Custom extension method speifically designed to integrate MvC5 with Autofac.
                                                                          //It will scan through project for controller and register those to Autofac.
            builder.RegisterApiControllers((typeof(MvcApplication).Assembly));

            /* Registering  a specific type: Datacomponent         */
            // builder.RegisterType<InMemoryRestaurantData>()
            builder.RegisterType<sqlRestaurantData>()
                 .As<IRestaurantData>()
                .InstancePerRequest();
            builder.RegisterType<OdeToFoodDbcontext>().InstancePerRequest();


            /*Creating container from ConatinerBuilder. This container we can give to MVC5 framework to resolve dependence*/
            var container = builder.Build();

            // Set dependency reslover throughout the project
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            httpconfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

        }
    }
}