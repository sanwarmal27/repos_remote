﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OdeToFood.Data.Models;

namespace OdeToFood.Data.Services
{
    public class sqlRestaurantData : IRestaurantData
    {
        private readonly OdeToFoodDbcontext db;

        public sqlRestaurantData(OdeToFoodDbcontext db)
        {
            this.db = db;
        }
        public void Add(Restaurant restaurant)
        {
            db.Restaurants.Add(restaurant);
            db.SaveChanges();
        }

        public void Delete(int id)
        {

            var restaurant = db.Restaurants.Find(id);
            db.Restaurants.Remove(restaurant);
            db.SaveChanges();
        }

        public Restaurant Get(int id)
        {
            return db.Restaurants.FirstOrDefault(r => r.id == id);

        }

        public IEnumerable<Restaurant> GetAll()
        {
            // return db.Restaurants.OrderBy(r=>r.Name);
            return from r in db.Restaurants
                   orderby r.id
                   select r;                                                

        }

        public void update(Restaurant restaurant)
        {
            var entry = db.Entry(restaurant);
            entry.State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
