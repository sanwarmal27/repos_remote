﻿using System;
using System.Collections.Generic;
using System.Linq;
using OdeToFood.Data.Models;

namespace OdeToFood.Data.Services
{
    public class InMemoryRestaurantData : IRestaurantData
    {
       List<Restaurant> restaurants;
        public InMemoryRestaurantData()
        {
            restaurants = new List<Restaurant>()
            {
                new Restaurant{id=1,Name="SCott's Pizza",Cusinie= CuisineType.Italian},
                new Restaurant{id=2,Name="Pizza Hut",Cusinie= CuisineType.French},
                new Restaurant{id=3,Name="Mango Grove",Cusinie= CuisineType.Indian}
            };
        }

        public void Add(Restaurant restaurant)
        {
            restaurants.Add(restaurant);
            restaurant.id = restaurants.Max(r => r.id) + 1;
        }

        public void Delete(int id)
        {
            var resturant = Get(id);
            if (resturant != null)
            {
                restaurants.Remove(resturant);
            }

        }

        public Restaurant Get(int id)
        {
            return restaurants.FirstOrDefault(r => r.id == id);
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return restaurants.OrderBy(r => r.Name);
        }

        public void update(Restaurant restaurant)
        {

            var existing = Get(restaurant.id);
            if (existing != null)
            {
                existing.Name = restaurant.Name;
                existing.Cusinie = restaurant.Cusinie;
            }
            
        }
    }

}
