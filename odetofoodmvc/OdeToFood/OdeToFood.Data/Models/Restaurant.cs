﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdeToFood.Data.Models
{
    public class Restaurant
    {
        public int id { get; set; }
        // Data Anotaion: Centrelised logic and Centralize the metadata
        //[RegularExpression]
        // [Range(1,20)]
        //[MaxLength (255)]
        [Required]
        public string Name { get; set; }

       // [DisplayFormat(DataFormatString =)]
       //[DisplayFormat(NullDisplayText =)]
       [Display(Name="Type of food")]
        public CuisineType Cusinie { get; set; }
    }
}
